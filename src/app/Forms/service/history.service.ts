import { Injectable } from "@angular/core";
import { Constants } from "./constants";

@Injectable({
    providedIn: 'root'
  })

  export class HistoryService   {

    private maxHistory:number = 10;
    private ids: number [] = [];


    getHistoryIds ():string [] {
        if (this.ids.length==0) return ["../assets/img/london.jpg"];
        return this.ids.map(x=>this.convertToLink(x));
    }

    convertToLink (id:number):string {
        return Constants.link.beginning+id+Constants.link.middle+id+Constants.link.ending;
      }

    addHistory (id:number):void {
        if (this.ids.length>=this.maxHistory) this.ids.splice (0,1);
        if (this.ids.filter(x=>x==id).length==0) this.ids.push (id);
      }

  }