import { Injectable } from "@angular/core";
import { Ids } from "src/app/Forms/ids";
import { Walpaper } from "src/app/service/walpaper";
import { default as walpapers }  from 'src/assets/json/wallpaper.json';
import { FlagService } from "./flag.service";

@Injectable({
    providedIn: 'root'
  })

  export class SelectionService   {
    ids:Ids = new Ids ();
    private walpapers: Walpaper [] =  walpapers.walpapers;

    constructor(private fs:FlagService) {}

    get selectedWalpaper (): number [] {
        return this.walpapers.filter (x=>this.fs.filterByFlags(x)).map (x=>x.id);
    }

    renderWithIntervals () {
        this.updateIds ();
        this.setByNextRandom ();
    }

    updateIds ():void {
        if (this.allIds.length!=this.selectedWalpaper.length) {
            this.ids.current=0;
            this.ids.previous = [];
            this.ids.next = this.selectedWalpaper;
        }
    }

    setByNextRandom ():void {
       if (this.ids.next.length>0) {
         let randomIndex:number = Math.floor((Math.random() * this.ids.next.length));
          if (this.ids.current!=0) this.ids.previous.push (this.ids.current);
          this.ids.current = this.ids.next [randomIndex];
          this.ids.next.splice (randomIndex,1);
        }
    }

    get allIds (): number [] {
       let a: number [] = [];
       a= a.concat (this.ids.previous);
       a.push (this.ids.current);
       a = a.concat (this.ids.next);
        return  a;
    }

    get zeroIndex (): number {
        return this.ids.previous.length;
    }

  }