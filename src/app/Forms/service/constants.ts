export class Constants {

    public static link = {
        beginning: 'https://images.pexels.com/photos/',
        middle:'/pexels-photo-',
        ending: '.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'
      }

}