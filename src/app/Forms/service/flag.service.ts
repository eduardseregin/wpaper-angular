import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, of } from "rxjs";
import { Walpaper } from "src/app/service/walpaper";
import { Mood } from "../mood";
import { Topic } from "../topic";


@Injectable({
    providedIn: 'root'
  })

  export class FlagService   {

    private season:string = "SPRING";
    private palette:string = "PASTEL";
    
    private mood = new BehaviorSubject (new Mood ());
    private currentMood = this.mood.asObservable();
    private moodComparison:Mood = new Mood ();

    private topic = new BehaviorSubject (new Topic ());
    private currentTopic = this.topic.asObservable();
    private topicComparison:Topic = new Topic ();

    private changed:boolean = false;

    clearInput () {
      this.season = "SPRING";
      this.palette = "PASTEL";
      this.topicComparison = new Topic ();
      this.moodComparison = new Mood ();
    }

    setSeason (season:string):void {
      if (this.season!=season) this.changed=true;  
      this.season = season;
    }

    setPalette (palette:string):void {
      if (this.palette!=palette) this.changed=true;  
      this.palette = palette;
    }

    setMood (mood:Mood):void {
      if (!this.equalMood (this.moodComparison, mood)) this.changed=true; 
      this.moodComparison = mood;
      this.mood.next (mood);
    }

    setTopic (topic:Topic):void {
      if (!this.equalTopic (this.topicComparison, topic)) this.changed=true; 
      this.topicComparison = topic;
      this.topic.next(topic);
    }

    getPalette ():string {
        return this.palette;
    }

    getSeason ():string {
      return this.season;
    }

    getMood () {
      return this.currentMood;
    }

    getMoodComparison () {
      return this.moodComparison;
    }

    getTopic () {                                           
      return this.currentTopic;                           
    }

    getTopicComparison () {
      return this.topicComparison;
    }


    isChanged ():boolean {
      let result = this.changed;
      this.changed = false;
      return result;
    }

    filterByFlags (wallpaper:Walpaper):boolean {
       
        let color:boolean;
        
        if (this.palette=="PASTEL" || this.palette=="SEPIA" || this.palette=="WHITE")
          color = this.palette==wallpaper.color;
          else color = wallpaper.color!="PASTEL" && wallpaper.color!="SEPIA" && wallpaper.color!="WHITE";

        let season:boolean;
        if (this.season=="AUTUMN" || this.season=="SPRING")
          season = this.season==wallpaper.season;
          else season = wallpaper.season!="AUTUMN" && wallpaper.season!="SPRING";
        
        if (color && season) {

          if (this.topicComparison.nature==true && wallpaper.nature ==false) return false;
          if (this.topicComparison.city==true && wallpaper.city ==false) return false;
          if (this.topicComparison.people==true && wallpaper.people ==false) return false;
          if (this.topicComparison.fantasy==true && wallpaper.fantasy ==false) return false;
          if (this.topicComparison.jokes==true && wallpaper.jokes ==false) return false;
          if (this.topicComparison.tech==true && wallpaper.tech ==false) return false;
          if (this.moodComparison.happy==true && wallpaper.happy ==false) return false;
          if (this.moodComparison.excited==true && wallpaper.excited ==false) return false;
          if (this.moodComparison.tender==true && wallpaper.tender ==false) return false;
          if (this.moodComparison.grim==true && wallpaper.grim ==false) return false;
          if (this.moodComparison.sad==true && wallpaper.sad ==false) return false;
        
          return true;
        }

        return false;
    }

    equalMood (compared:Mood, other:Mood):boolean {
      let h:boolean = compared.happy==other.happy;
      let e:boolean = compared.excited==other.excited;
      let t:boolean = compared.tender==other.tender;
      let g:boolean = compared.grim==other.grim;
      let s:boolean = compared.sad==other.sad;

      return h && e && t && g && s;
    }

    equalTopic (compared:Topic, other:Topic):boolean {

      let n:boolean = compared.nature==other.nature;
      let c:boolean = compared.city==other.city;
      let p:boolean = compared.people==other.people;
      let f:boolean = compared.fantasy==other.fantasy;
      let j:boolean = compared.jokes==other.jokes;
      let t:boolean = compared.tech==other.tech;

      return n && c && p && f && j && t;

    }

  }