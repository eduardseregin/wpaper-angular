import { Component } from '@angular/core';
import {  MatSliderChange } from '@angular/material/slider';
import { FlagService } from '../service/flag.service';

@Component({
  selector: 'app-palette',
  templateUrl: './palette.component.html',
  styleUrls: ['./palette.component.css']
})
export class PaletteComponent  {

    constructor(private fs:FlagService) { }

    onSliderChange (event:MatSliderChange):void {
      switch (event.value) {
      case 1: this.fs.setPalette ("COLOR"); break;
      case 2: this.fs.setPalette ("PASTEL");; break;
      case 3: this.fs.setPalette ("SEPIA");; break;
      default: this.fs.setPalette ("WHITE");;

    }
  }

sliderSelectedClass (target: string):string {
  if (target===this.fs.getPalette()) return "label tag-selected";
  return "label tag";
}

get initialValue ():number {
 let l:string = this.fs.getPalette().charAt(0);
    switch (l){
      case "C": return 1;
      case "P": return 2;
      case "S": return 3;
    }                                   
  return 4;
}

}
