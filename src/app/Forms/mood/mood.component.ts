import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Mood } from '../mood';
import { FlagService } from '../service/flag.service';

@Component({
  selector: 'app-mood',
  templateUrl: './mood.component.html',
  styleUrls: ['./mood.component.css']
})
export class MoodComponent implements OnInit {

  fbM: FormGroup;

  constructor(private fb:FormBuilder, private fs:FlagService) { }
  
  ngOnInit(): void {
    this.formInit();
  }

  formInit() {
    let m:Mood = this.fs.getMoodComparison();
    this.fbM = this.fb.group(m);
  }

  updateMood(): void {
    let newMood:Mood =  
    {
      happy: this.fbM.get ('happy').value,
      excited: this.fbM.get ('excited').value,
      tender: this.fbM.get ('tender').value,
      grim: this.fbM.get ('grim').value,
      sad: this.fbM.get ('sad').value
    };

    this.fs.setMood (newMood);
  }


}
