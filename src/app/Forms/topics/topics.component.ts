import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FlagService } from '../service/flag.service';
import { Topic } from '../topic';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.css']
})
export class TopicsComponent implements OnInit {

  fbT: FormGroup;

  constructor(private fb:FormBuilder, private fs:FlagService) { }
  
  ngOnInit(): void {
    this.formInit();
  }

  formInit() {
    let t:Topic = this.fs.getTopicComparison();
    this.fbT = this.fb.group(t);

  }

  updateTopic(): void {
    let newTopic:Topic = 
    {
      nature: this.fbT.get ('nature').value,
      city: this.fbT.get ('city').value,
      people: this.fbT.get ('people').value,
      fantasy: this.fbT.get ('fantasy').value,
      jokes: this.fbT.get ('jokes').value,
      tech: this.fbT.get ('tech').value
    };

    this.fs.setTopic (newTopic);
  }



}
