export class Mood {

    constructor() {
      
      this.happy = false;
      this.excited = false;
      this.tender = false;
      this.grim = false;
      this.sad = false;
    }
  
    happy: boolean;
    excited: boolean;
    tender: boolean;
    grim: boolean;
    sad: boolean;


  }
