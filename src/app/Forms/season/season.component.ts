import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import { FlagService } from '../service/flag.service';
import { Topic } from '../topic';


@Component({
  selector: 'app-season',
  templateUrl: './season.component.html',
  styleUrls: ['./season.component.css']
})
export class SeasonComponent implements OnInit {

  topic:Topic;
  season:FormControl=new FormControl(this.fs.getSeason ()); 

  constructor(@Inject(DOCUMENT) private document: Document, private fs:FlagService) { }
  

  ngOnInit(): void {
    this.fs.getTopic ().subscribe((x)=>{
      this.topic=x;
  }); 
  }
  
updateSeason ():void {
 this.fs.setSeason (this.season.value);
}

get myTopic () {
  return this.topic;
}

clear () {
  this.fs.clearInput();
  this.document.location.reload();
}

}
