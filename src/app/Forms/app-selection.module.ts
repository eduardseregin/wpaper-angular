import { NgModule } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { ReactiveFormsModule } from '@angular/forms';

import { SelectionComponent } from 'src/app/Forms/selection/selection.component';
import { TopicsComponent } from './topics/topics.component';
import { PaletteComponent } from './palette/palette.component';
import { SeasonComponent } from './season/season.component';
import { MoodComponent } from './mood/mood.component';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';


@NgModule({
  declarations: [SelectionComponent,
    TopicsComponent,
    PaletteComponent, 
    SeasonComponent, 
    MoodComponent],
  exports: [SelectionComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatSliderModule
    
  ],
  providers: [ ]
})
export class AppSelectionModule { }
