import { Component, OnInit } from '@angular/core';
import { Constants } from '../service/constants'
import { IdService } from '../../service/id.service';
import { HistoryService } from '../service/history.service';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.css']
})
export class SelectionComponent implements OnInit {

  private pictId:number;

   constructor(private idService: IdService, private hs:HistoryService) { }


  ngOnInit(): void {
   
    // 1. subscribe for updates in pictures
    this.idService.idForSubscribe().subscribe((x)=>{
      this.pictId=x;  }); 
   
      // 2. set update interval
    setInterval (
      ()=>{this.idService.updateIdWithInterval();}, 5000 );
  }

  
 get pictureLink ():string {
  if (this.pictId<1000) return "../assets/img/london.jpg";
  let pict:number = this.pictId;
  return Constants.link.beginning+pict+Constants.link.middle+pict+Constants.link.ending;
 }

 get downloadLink ():string {
  if (this.pictId<1000) return "../assets/img/london.jpg";
  let pict:number = this.pictId; 
  return Constants.link.beginning+pict+Constants.link.middle+pict+".jpeg";
 }


// 3. Start - Stop of changes in picture IDs
 pressSearchButton(): void {
  this.idService.changeSearchButton ();
 }

searchButtonClass ():string {
    if (this.idService.isOnSearchButton) { 
        return "icon-item search-button-pressed";
      }
    return "icon-item search-button";
  }

  pressLeftAngleButton ():void {
    this.idService.isOnSearchButton = false;
    this.idService.goPreviousId ();
  }

  pressRightAngleButton ():void {
    this.idService.isOnSearchButton = false;
    this.idService.goNextId ();
  }

  pressDownloadRef ():void {
    this.idService.isOnSearchButton = false;
    this.hs.addHistory (this.pictId);
  }

  get isExistPrevious ():boolean {
    return this.idService.isPreviousIdExist();
  }

  get isExistNext ():boolean {
    return this.idService.isNextIdExist();
  }

}
