import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { MatCarouselModule } from 'ng-mat-carousel';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MainComponent } from './main/main.component';
import { HistoryComponent } from './history/history.component';
import { FooterComponent } from './footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { AboutComponent } from './about/about.component';

import { AppSelectionModule } from './Forms/app-selection.module';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainComponent,
    HistoryComponent,
    FooterComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
     MatCarouselModule.forRoot(),
    AppRoutingModule,
    AppSelectionModule
    ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
