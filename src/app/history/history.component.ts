import { Component  } from '@angular/core';
import { HistoryService } from '../Forms/service/history.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent {

  constructor(private hs:HistoryService) { }

   get history ():string [] {
     return this.hs.getHistoryIds();
  }


 
}
