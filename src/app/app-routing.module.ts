import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { HistoryComponent } from './history/history.component';
import { AboutComponent } from './about/about.component';

import { AppSelectionModule } from './Forms/app-selection.module';
import { SelectionComponent } from './Forms/selection/selection.component';

const routes: Routes = [
  { path: '', redirectTo: 'Home', pathMatch: 'full' },
  { path: 'Home', component: MainComponent },
  { path: 'About', component: AboutComponent },
  { path: 'Mood', component: SelectionComponent },
  { path: 'History', component: HistoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
