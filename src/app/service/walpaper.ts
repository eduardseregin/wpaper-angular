export class Walpaper {

    constructor(id: number, color: string, season: string, city: boolean,
        people: boolean, fantasy: boolean, jokes: boolean, films: boolean, 
        tech: boolean, happy: boolean, grim: boolean, sad: boolean) {
      this.id = id;
      this.color = color;
      this.season = season;
      this.city = city;
      this.people = people;
      this.fantasy = fantasy;
      this.jokes = jokes;
      this.films = films;
      this.tech = tech;
      this.happy = happy;
      this.grim = grim;
      this.sad = sad;
    }
  
    id: number;
    color: string;
    season: string;
    nature: boolean;
    city: boolean;
    people: boolean;
    fantasy: boolean;
    jokes: boolean;
    films: boolean;
    tech: boolean;
    happy: boolean;
    excited: boolean;
    tender: boolean;
    grim: boolean;
    sad: boolean;
  }
