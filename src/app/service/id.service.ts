import { Injectable  } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Ids } from '../Forms/ids';
import { SelectionService } from '../Forms/service/selection.service';

@Injectable({
  providedIn: 'root'
})
export class IdService   {
  
  public isOnSearchButton: boolean = false; 
  
  private navigatorIndex: number = 0;
  
  // VERY IMPORTANT
  private idSource = new BehaviorSubject (105);
  private currentId = this.idSource.asObservable (); 

      
    constructor(private ss:SelectionService) { }

    //Preparatory Steps

    idForSubscribe () {
       return this.currentId;
     }
    
     updateIdWithInterval ():void {
    
      if (this.isOnSearchButton) {
         this.ss.renderWithIntervals ();

         if (this.navigatorIndex==0) this.emitId (0);
    }
  }

  emitId (navIndex:number):void {
    let id:number = this.ss.allIds [this.ss.zeroIndex + navIndex];
  
    this.idSource.next (id);
  }


// For Use of the Service

  changeSearchButton ():void {
    this.isOnSearchButton=!this.isOnSearchButton;
    if (this.isOnSearchButton) {
      this.navigatorIndex = 0;
    }
  }


  goPreviousId ():void {
    this.navigatorIndex--;
   this.emitId (this.navigatorIndex);
 
  }

  goNextId ():void {
    this.navigatorIndex++;
    this.emitId (this.navigatorIndex);
  }

  isNextIdExist ():boolean {
    let size:number = this.ss.allIds.length;
    let navInd:number = this.navigatorIndex + this.ss.zeroIndex;
  return size>navInd+1; 
  }

  isPreviousIdExist ():boolean {
   let navInd:number = this.navigatorIndex + this.ss.zeroIndex;
  return navInd>0;
  }

}

